package com.sepehr.hoshmand.fetrie.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sepehr.hoshmand.fetrie.MainActivity;
import com.sepehr.hoshmand.fetrie.R;
import com.sepehr.hoshmand.fetrie.general.G;
import com.sepehr.hoshmand.fetrie.views.Txt;


public class SyadatFragment extends Fragment {

    LinearLayout seyyedLay;
    LinearLayout notSeyyedLay;
    ImageView seyyedImgvu;
    Txt seyyedTxtvu;
    ImageView notSeyyedImgvu;
    Txt notSeyyedTxtvu;


    public SyadatFragment() {
        // Required empty public constructor
    }
    public static SyadatFragment newInstance() {
        SyadatFragment fragment = new SyadatFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fView = inflater.inflate(R.layout.fragment_seyadat, container, false);

        seyyedLay = (LinearLayout) fView.findViewById(R.id.seyyed_lay);
        notSeyyedLay = (LinearLayout) fView.findViewById(R.id.not_seyyed_lay);


        seyyedImgvu = (ImageView) seyyedLay.findViewById(R.id.item_imgvu);
        seyyedTxtvu = (Txt) seyyedLay.findViewById(R.id.item_txtvu);


        notSeyyedImgvu = (ImageView) notSeyyedLay.findViewById(R.id.item_imgvu);
        notSeyyedTxtvu = (Txt) notSeyyedLay.findViewById(R.id.item_txtvu);

        seyyedImgvu.setBackgroundResource(R.drawable.seyyed);
        seyyedTxtvu.setText(getResources().getString(R.string.seyyed));

        notSeyyedImgvu.setBackgroundResource(R.drawable.seyyed_not);
        notSeyyedTxtvu.setText(getResources().getString(R.string.notSeyyed));

        seyyedLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                G.getInstance().getFetrie().setSeyyed(true);
                ((MainActivity)getActivity()).gotoNextPage();

            }
        });

        notSeyyedLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                G.getInstance().getFetrie().setSeyyed(false);
                ((MainActivity)getActivity()).goto2NextPage();

            }
        });


        return fView;
    }
}
