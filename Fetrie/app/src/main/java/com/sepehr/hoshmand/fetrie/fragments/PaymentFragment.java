package com.sepehr.hoshmand.fetrie.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.sepehr.hoshmand.fetrie.ChooseServiceActivity;
import com.sepehr.hoshmand.fetrie.MainActivity;
import com.sepehr.hoshmand.fetrie.Networks.HttpResult;
import com.sepehr.hoshmand.fetrie.Networks.NameValuePair;
import com.sepehr.hoshmand.fetrie.Networks.NetworkHelper;
import com.sepehr.hoshmand.fetrie.Networks.ServiceCallback;
import com.sepehr.hoshmand.fetrie.R;
import com.sepehr.hoshmand.fetrie.general.G;
import com.sepehr.hoshmand.fetrie.general.SignController;
import com.sepehr.hoshmand.fetrie.object.ServerFetrie;
import com.sepehr.hoshmand.fetrie.views.EditTxt;
import com.sepehr.hoshmand.fetrie.views.Fbtn;
import com.sepehr.hoshmand.fetrie.views.Txt;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.StringReader;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import ir.fanap.psp.fanapinapppayment.model.FanapRequestPay;
import ir.fanap.psp.fanapinapppayment.model.FanapResponsePay;
import ir.fanap.psp.fanapinapppayment.service.FanapMpgService;

import static javax.crypto.Cipher.PRIVATE_KEY;


public class PaymentFragment extends Fragment {

    private Txt foodTxt;
    private Txt syadataTxt;
    private Txt paytoTxt;
    private Txt provinceTxt;
    private Txt costPerPersonTxt;
    private Txt personCountTxt;
    private Txt institudeTxt;
    private Txt totalCostTxt;
    private Fbtn submitBtn;
    private EditTxt phoneEditTxt;

    private String generatedUUD;
    String userPhone;


    int merchantId = 903980;
    int terminalId = 901280;
    int company = 903982;
    String serverAns;
    Messenger mMessenger;


    public PaymentFragment() {
        // Required empty public constructor

    }

    public static PaymentFragment newInstance() {
        PaymentFragment fragment = new PaymentFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fView = inflater.inflate(R.layout.fragment_payment, container, false);

        foodTxt = (Txt) fView.findViewById(R.id.food_txtvu);
        syadataTxt = (Txt) fView.findViewById(R.id.seyadat_txtvu);
        paytoTxt = (Txt) fView.findViewById(R.id.pay_to_txtvu);
        provinceTxt = (Txt) fView.findViewById(R.id.province_txtvu);
        costPerPersonTxt = (Txt) fView.findViewById(R.id.price_of_person_txtvu);
        personCountTxt = (Txt) fView.findViewById(R.id.person_count_txtvu);
        institudeTxt = (Txt) fView.findViewById(R.id.institude_txtvu);
        totalCostTxt = (Txt) fView.findViewById(R.id.complete_price_txtvu);
        submitBtn = (Fbtn) fView.findViewById(R.id.submit_btn);
        phoneEditTxt = (EditTxt) fView.findViewById(R.id.payment_phone_number);

        setValues();

        mMessenger = new Messenger(new ResponseHandler());
        initMPGCall();

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String encodedHash = Uri.encode("#");
//                Intent intent = new Intent(Intent.ACTION_DIAL);
//                intent.setData(Uri.parse("tel:*770*880*1000"+encodedHash));
//                PaymentFragment.this.getActivity().startActivity(intent);

                userPhone = ""+phoneEditTxt.getText();
                if(!userPhone.startsWith("09") || userPhone.length()!=11){
                    Toast.makeText(PaymentFragment.this.getActivity(),"شماره تلفن همراه اشتباه وارد شده",Toast.LENGTH_LONG).show();
                }else {
                    generatedUUD = UUID.randomUUID().toString();
                    sendServerRequest();
                }


//                generateSignedString(frp);
            }
        });

        return fView;
    }

    public void setValues() {

        foodTxt.setText(G.getInstance().fetrieToString(getActivity(),G.getInstance().getFetrie()).getFoodType());
        syadataTxt.setText(G.getInstance().fetrieToString(getActivity(),G.getInstance().getFetrie()).getIsSeyyed());
        paytoTxt.setText(G.getInstance().fetrieToString(getActivity(),G.getInstance().getFetrie()).getPaytoSeyyed());
        provinceTxt.setText(G.getInstance().fetrieToString(getActivity(),G.getInstance().getFetrie()).getProvince()+","+
                G.getInstance().fetrieToString(getActivity(),G.getInstance().getFetrie()).getCity());
        costPerPersonTxt.setText(G.getInstance().fetrieToString(getActivity(),G.getInstance().getFetrie()).getCost());
        personCountTxt.setText(G.getInstance().fetrieToString(getActivity(),G.getInstance().getFetrie()).getPersonNum());
        institudeTxt.setText(G.getInstance().fetrieToString(getActivity(),G.getInstance().getFetrie()).getInstitude());
        ServerFetrie sf = G.getInstance().getFetrie();
        totalCostTxt.setText(sf.getCost()*sf.getPersonNum()+" تومان ");
    }

    private void sendServerRequest() {
        JSONObject object = new JSONObject();
        ServerFetrie fetrie = G.getInstance().getFetrie();

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

        try {
            if(fetrie.getFoodType() == 0) {
                nameValuePairs.add(new NameValuePair("food_type", "WH"));
            }else{
                nameValuePairs.add(new NameValuePair("food_type", "RI"));
            }
            if(fetrie.isSeyyed()) {
                nameValuePairs.add(new NameValuePair("is_seyed", "1"));
            }else{
                nameValuePairs.add(new NameValuePair("is_seyed", "0"));
            }

            if(fetrie.isPaytoSeyyed()){
                nameValuePairs.add(new NameValuePair("to_seyed", "1"));
            }else{
                nameValuePairs.add(new NameValuePair("to_seyed", "0"));
            }


            nameValuePairs.add(new NameValuePair("family_num", fetrie.getPersonNum()+""));
            nameValuePairs.add(new NameValuePair("fee", fetrie.getCost()+""));
            nameValuePairs.add(new NameValuePair("institute", convertToServerIns(fetrie.getInstitude())+""));
            nameValuePairs.add(new NameValuePair("province", fetrie.getProvince()+""));
            nameValuePairs.add(new NameValuePair("city", fetrie.getCity()+""));
            nameValuePairs.add(new NameValuePair("UUID",generatedUUD));
            nameValuePairs.add(new NameValuePair("signature",SignController.SignString(generatedUUD)));

        } catch (Exception e) {
            e.printStackTrace();
        }




        String url = G.BASE_URL + "fetrie_settings";

        AsyncTask<String, String, HttpResult> postService = NetworkHelper.sendPostRequest(getActivity(), url, nameValuePairs, new ServiceCallback() {
            @Override
            public void onTaskComplete(HttpResult result) {
                Log.d("ser Ans", result.getContent());
                if((result.getStatus() == 200) && result.getContent().length()>0) {
                    try {
                        serverAns = result.getContent();
                        int k = Integer.valueOf(serverAns);

                        Bundle bundle = new Bundle();
                        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "fetrieh_payment");
                        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "click_before_payment--"+k);
                        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "text");
                        ChooseServiceActivity.mFirebaseAnalytics.logEvent("payment_start", bundle);

                        purchase();
                    }catch (Exception e){
                        Toast.makeText(getActivity(),"خطا در ارسال اطلاعات",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getActivity(),"اشکال در ارتباط با سرور",Toast.LENGTH_LONG).show();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private int convertToServerIns(int institude) {
        if(institude == 0){
            return 3;
        }else if(institude == 1){
            return 1;
        }else{
            return 0;
        }

    }


    //////////////Fanap Payment

    ResponseHandler responseHandler;
    ServiceConnection serviceConnection;
    Messenger serviceMessenger;

    private void initMPGCall(){

        responseHandler = new ResponseHandler();
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                serviceMessenger = new Messenger(iBinder);
//                mBound = true;
            }
            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                serviceMessenger = null;
//                mBound = false;
            }
        };
        PaymentFragment.this.getActivity().bindService(new Intent(PaymentFragment.this.getActivity(), FanapMpgService.class), serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private String generateSignedString(FanapRequestPay data){
//        FanapRequestPay fanapRequestPay = new FanapRequestPay();
//        fanapRequestPay.setData(data);
        String signString = "" + data.getMerchantId() + "|" +
                data.getTerminalId() + "|" + data.getUserMobile() +
                "|" + data.getAmount() + "|"  + data.getUuid();
        String modulusElem = "", dElem = "";
        byte[] modulusBytes = null, dBytes = null;
        BigInteger modulus = null, d = null;

        modulusElem = "2/fQQxCERv36KJCft3bMcUwy05Tjv+AxyYtaw3gtTuMc7nmZbCKuAlfzXB1iGvXiJdwyiwmixq3VvyTjqyCv1FUOFSjuMP3xBmtgiyVh5VDXFa4ZLPs2IkmzQUJX5KAbhRq7eCD/LXqKB6cZilXqnMCJQvQjUqkHE3uCO50PDEM=";
        dElem = "rhifLtU9vGp7uettXemauflTKP7omw64mQnAloDha1R5q412jo2MJ7KwnDFtWmVNziVPo5fGy3T+fcCWqPS/89ezjosgsPjYyPD9qwUJ6NyjzNKLPgB1ZYbV6lV0ZzI3RMZ/vJfkJMMpo2kQGF9SsCRsWbBrELM4lY5F6Jsse+E=";
        Base64 base64 = new Base64();
        modulusBytes = base64.decodeBase64(modulusElem.getBytes());
        dBytes = base64.decodeBase64(dElem.getBytes());
        modulus = new BigInteger(1, modulusBytes);
        d = new BigInteger(1, dBytes);
        String sign = sign(signString, modulus, d);
        return sign;
    }

    public String sign(String data, BigInteger modulus, BigInteger d) {
        try {
            Signature signature = Signature.getInstance("SHA1withRSA");
            KeyFactory factory = KeyFactory.getInstance("RSA");
            RSAPrivateKeySpec privateKeySpec = new RSAPrivateKeySpec(modulus, d);
            PrivateKey privateKey = factory.generatePrivate(privateKeySpec);
            signature.initSign(privateKey); signature.update(data.getBytes("UTF-8"));
            byte[] signedByteData = signature.sign();
            Base64 base64 = new Base64();
            return new String (base64.encode(signedByteData));
        } catch (Exception ex) {
            ex.printStackTrace();
            return ""; }
    }

    class ResponseHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            int respCode = msg.what;
            switch (respCode) {
                case FanapMpgService.CBAZAAR_RESPONSE_PAYMENT: {
                    String result = msg.getData().getString(FanapMpgService.RESPONSE_DATA);
                    FanapResponsePay response = new Gson().fromJson(result,
                            FanapResponsePay.class);
                    String resp = response.getMessage();

                    Bundle bundle = new Bundle();
                    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "fetrieh_payment");
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "payment_done___"+resp);
                    bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "text");
                    ChooseServiceActivity.mFirebaseAnalytics.logEvent("payment_done", bundle);

                    sendDeliveryRequest(response,serverAns);



//                    textViewAmountRes.setText(response.getAmount());
//                    textViewMessageRes.setText(response.getMessage());
//                    textViewRefNumberRes.setText(response.getReferenceNumber());
//                    textViewResultCodeRes.setText(response.getResCode());
//                    textViewUserIdRes.setText(response.getUserId());
//                    textViewUuid.setText(response.getUuid());
                } default:
            }
            super.handleMessage(msg);
        }
    }

    private void sendDeliveryRequest(final FanapResponsePay response, String serverAns) {

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

        try {

            String responseText = "Amount-"+response.getAmount()+"_Merchant-"+response.getMerchant()+"_Message-"+response.getMessage()+
                    "_Pan-"+response.getPan()+"_RefNum-"+response.getReferenceNumber()+"_Terminal-"+response.getTerminal()+
                    "_TransDate-"+response.getTransDate()+"_UserId-"+response.getUserId()+"_UserMobile-"+response.getUserMobile()+
                    "_UUID-"+response.getUuid();
            nameValuePairs.add(new NameValuePair("id", serverAns));
            nameValuePairs.add(new NameValuePair("response_code", response.getResCode()));
            nameValuePairs.add(new NameValuePair("response_text", responseText));
            nameValuePairs.add(new NameValuePair("signature", SignController.SignString(serverAns)));

        } catch (Exception e) {
            e.printStackTrace();
        }

        String url = G.BASE_URL + "payment_delivery";

        AsyncTask<String, String, HttpResult> postService = NetworkHelper.sendPostRequest(getActivity(), url, nameValuePairs, new ServiceCallback() {
            @Override
            public void onTaskComplete(HttpResult result) {
                Log.d("ser Ansdelivery", result.getContent());
                if(result.getStatus() == 200) {
                    Gson gson = new Gson();
                    String param = gson.toJson(response);

                    ((MainActivity)getActivity()).setFactor(response);
                    ((MainActivity)getActivity()).gotoNextPage();


                }else{

                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    void purchase()
    {
        Message msg = Message.obtain(null, FanapMpgService.WHAT_CBAZAAR_REQUEST_PAYMENT);

        msg.replyTo = mMessenger;
        Bundle bundle = new Bundle();
//        FanapRequestPay fanapRequestPay = new FanapRequestPay();
        //>>>>>> FILL fanapRequestPay fields here...

        FanapRequestPay frp = new FanapRequestPay();
        frp.setAmount(""+(G.getInstance().getFetrie().getCost()*G.getInstance().getFetrie().getPersonNum()*10));
        frp.setCompany(company);
        frp.setMerchantId(merchantId);
        frp.setTerminalId(terminalId);
        frp.setUserMobile(""+userPhone/*"09359970247"*/);
        frp.setUuid(generatedUUD);
        frp.setSign(generateSignedString(frp));


        String reqData = new Gson().toJson(frp);
        bundle.putString(FanapMpgService.REQUEST_DATA, reqData);
        bundle.putString(FanapMpgService.SUPPORT_NUMBER, userPhone+""/*"09359970247"*/);
        bundle.putString(FanapMpgService.PACKAGE_NAME, PaymentFragment.this.getActivity().getApplicationContext().getPackageName());
        msg.setData(bundle);
        try {
            serviceMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }

        int test = 4;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(serviceConnection != null){
            try {
                PaymentFragment.this.getActivity().unbindService(serviceConnection);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private PrivateKey loadPrivateKey(String key64) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] clear = android.util.Base64.decode(key64, android.util.Base64.DEFAULT);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(clear);
        KeyFactory fact = KeyFactory.getInstance("RSA");
        PrivateKey priv = fact.generatePrivate(keySpec);
        Arrays.fill(clear, (byte) 0);
        return priv;
    }

    public static String getBase64StrFromByte(byte[] key){
        if (key == null || key.length == 0) return null;
        return new String(android.util.Base64.encode(key, android.util.Base64.DEFAULT));
    }
}
