package com.sepehr.hoshmand.fetrie.Networks;

import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class NetworkService extends AsyncTask<String, String, HttpResult> {

    private String url;
    private ServiceCallback callback;

    private JSONObject body;
    private HttpMethod httpMethod;
    private List<ReqHeader> headers;


    public NetworkService() {

    }

    public NetworkService(String url, List<ReqHeader> headers,
                          ServiceCallback callback) {
        this(url, null, HttpMethod.GET, headers, callback);
    }

    public NetworkService(String url, JSONObject body, HttpMethod httpMethod
            , List<ReqHeader> headers, ServiceCallback callback) {
        AssertArguments.notNull(url, "url");
        AssertArguments.notNull(callback, "callback");
        this.url = url;
        this.body = body;
        this.callback = callback;
        this.httpMethod = httpMethod;
        this.headers = headers;

    }

    @Override
    protected HttpResult doInBackground(String... arg0) {

//        int status = 0;
//        String content = "";
//        BufferedReader reader = null;
//        try {
//            StringBuilder builder = new StringBuilder();
//
//            HttpRequestBase httpRequest;
//            if(this.httpMethod == HttpMethod.GET){
//                httpRequest = new HttpGet();
//            }else{
//                httpRequest = new HttpPost();
//            }
//
//            HttpClient httpClient = new DefaultHttpClient();
//            HttpPost request = new HttpPost(url);
//            HttpParams httpParameters = new BasicHttpParams();
//            int timeoutConnection = G.getInstance().CONNECTION_TIME_OUT;
//            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
//            request.setParams(httpParameters);
//            StringEntity e;
//            e = new StringEntity(body.toString(), "UTF-8");
//            request.setEntity(e);
//
//            request.setHeader(HTTP.CONTENT_TYPE, "application/json");
//            request.setHeader("Authorization", "Bearer " + G.getInstance().getDataBaseInfo().getTokenDao().loadAll().get(0).getToken());
//            PackageInfo pInfo = G.getInstance().getContext().getPackageManager().getPackageInfo(G.getInstance().getContext().getPackageName(), 0);
//            request.setHeader("client-id", context.getResources().getString(R.string.client_id));
//            request.setHeader("version-code", Integer.toString(pInfo.versionCode));
//            request.setHeader("api-level", Integer.toString(1));
//            request.setHeader("device-id", DeviceUtils.getAndroidId(G.getInstance().getContext()));
//            Log.d("device-id = ", DeviceUtils.getAndroidId(G.getInstance().getContext()));
//            request.addHeader("Accept-Encoding", "gzip");
//            HttpResponse response = httpClient.execute(request);
//            status = response.getStatusLine().getStatusCode();
//
//            if (status < 300) {
//                HttpEntity ex = response.getEntity();
//                // String data = EntityUtils.toString(e);
//
//                Header encoding = response.getFirstHeader("Content-Encoding");
//                boolean gzipped = encoding != null && encoding.getValue().toLowerCase().contains("gzip");
//
//                InputStream content2 = gzipped ? new GZIPInputStream(ex.getContent()) : ex.getContent();
//                reader = new BufferedReader(new InputStreamReader(content2));
//
//                byte[] bytes = new byte[1000];
//
//                StringBuilder x = new StringBuilder();
//
//                int numRead = 0;
//                while ((numRead = content2.read(bytes)) >= 0) {
//                    x.append(new String(bytes, 0, numRead));
//                }
//
//                content = x.toString();
//
////                String line;
////                while ((line = reader.readLine()) != null) {
////                    builder.append(line);
////                }
////
////                content = builder.toString();
//            } else {
//
//                HttpEntity ex = response.getEntity();
//                // String data = EntityUtils.toString(e);
//                InputStream content2 = ex.getContent();
//                reader = new BufferedReader(new InputStreamReader(content2));
//
//                byte[] bytes = new byte[1000];
//
//                StringBuilder x = new StringBuilder();
//
//                int numRead = 0;
//                while ((numRead = content2.read(bytes)) >= 0) {
//                    x.append(new String(bytes, 0, numRead));
//                }
//
//                content = x.toString();
//
////                while ((line = reader.readLine()) != null) {
////                    builder.append(line);
////                }
////
////                content = builder.toString();
//
//                try {
//                    Exception exception = new Exception("PostWithTokenService-doInbackground-status = " + status + " - response = " + response + " - url = " + url);
//                    G.getInstance().getLogger().logExceptions(exception);
//                } catch (Exception exe) {
//                    Exception exception = new Exception("onlogException ", exe);
//                    G.getInstance().getLogger().logExceptions(exception);
//                }
//                if(content == null){
//                    content = "";
//                }
//            }
//
//        } catch (Exception ex) {
//            try {
//                Exception exception = new Exception("PostWithTokenService-doInbackground-status = " + status + " - url = " + url, ex);
//                G.getInstance().getLogger().logExceptions(exception);
//            } catch (Exception e) {
//                Exception exception = new Exception("onlogException ", e);
//                G.getInstance().getLogger().logExceptions(exception);
//            }
//        } finally {
//            try {
//                if (reader != null) {
//                    reader.close();
//                }
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }

        String content = "";
        int status = 0;
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
        Request.Builder builder;
        builder = new Request.Builder()
                .url(url);
        if (httpMethod != HttpMethod.GET) {
            builder = setBody(builder);
        }

        for (int i = 0; i < headers.size(); i++) {
            builder.addHeader(headers.get(i).getName(), headers.get(i).getValue());
        }
        Request request = builder.build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            status = response.code();
            content = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new HttpResult(status, content);

    }

    public Request.Builder setBody(Request.Builder builder) {
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, this.body.toString());

        return builder.post(requestBody);
    }


    @Override
    protected void onPostExecute(HttpResult result) {

        callback.onTaskComplete(result);
        callback.onTaskComplete(result.getContent(), result.getStatus());
        // Dialog.dismiss();
        try {
            NetworkService.this.get(20000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        super.onPostExecute(result);
    }

    private final OkHttpClient client = new OkHttpClient();

    public void run() throws Exception {
        Request request = new Request.Builder()
                .url("http://publicobject.com/helloworld.txt")
                .build();
        client.newCall(request).execute().body().string();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                Headers responseHeaders = response.headers();
                for (int i = 0, size = responseHeaders.size(); i < size; i++) {
                    System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
                }

                System.out.println(response.body().string());
            }
        });
    }
}
