package com.sepehr.hoshmand.fetrie.fragments.kaffare;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.sepehr.hoshmand.fetrie.ChooseServiceActivity;
import com.sepehr.hoshmand.fetrie.KaffareActivity;
import com.sepehr.hoshmand.fetrie.MainActivity;
import com.sepehr.hoshmand.fetrie.Networks.HttpResult;
import com.sepehr.hoshmand.fetrie.Networks.NameValuePair;
import com.sepehr.hoshmand.fetrie.Networks.NetworkHelper;
import com.sepehr.hoshmand.fetrie.Networks.ServiceCallback;
import com.sepehr.hoshmand.fetrie.R;
import com.sepehr.hoshmand.fetrie.fragments.PaymentFragment;
import com.sepehr.hoshmand.fetrie.general.G;
import com.sepehr.hoshmand.fetrie.general.SignController;
import com.sepehr.hoshmand.fetrie.object.Kaffare;
import com.sepehr.hoshmand.fetrie.object.ServerFetrie;
import com.sepehr.hoshmand.fetrie.object.ServerKaffare;
import com.sepehr.hoshmand.fetrie.views.EditTxt;
import com.sepehr.hoshmand.fetrie.views.Fbtn;
import com.sepehr.hoshmand.fetrie.views.Txt;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.RSAPrivateKeySpec;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import ir.fanap.psp.fanapinapppayment.model.FanapRequestPay;
import ir.fanap.psp.fanapinapppayment.model.FanapResponsePay;
import ir.fanap.psp.fanapinapppayment.service.FanapMpgService;


public class KPaymentFragment extends Fragment {

    private Txt kaffare_type_Txt;
    private Txt days_count_txt;
    private Txt cost_per_day_txt;
    private Txt institute_txt;
    private Txt city_txt;
    private Txt totalCostTxt;
    private Fbtn submitBtn;
    private EditTxt phoneEditTxt;

    int merchantId = 903980;
    int terminalId = 901280;
    int company = 903982;
    String serverAns;
    Messenger mMessenger;

    private String generatedUUD;
    String userPhone;



    public KPaymentFragment() {
        // Required empty public constructor
    }

    public static KPaymentFragment newInstance() {
        KPaymentFragment fragment = new KPaymentFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fView = inflater.inflate(R.layout.fragment_kaffare_payment, container, false);

        kaffare_type_Txt = (Txt) fView.findViewById(R.id.kaffare_type_txtvu);
        days_count_txt = (Txt) fView.findViewById(R.id.days_count_txtvu);
        cost_per_day_txt = (Txt) fView.findViewById(R.id.cost_txtvu);
        institute_txt = (Txt) fView.findViewById(R.id.institite_txtvu);
        totalCostTxt = (Txt) fView.findViewById(R.id.complete_price_txtvu);
        submitBtn = (Fbtn) fView.findViewById(R.id.submit_btn);
        city_txt = (Txt) fView.findViewById(R.id.city_txtvu);
        phoneEditTxt = (EditTxt) fView.findViewById(R.id.payment_phone_number);

        setValues();

        mMessenger = new Messenger(new KPaymentFragment.ResponseHandler());
        initMPGCall();

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userPhone = ""+phoneEditTxt.getText();
                if(!userPhone.startsWith("09") || userPhone.length()!=11){
                    Toast.makeText(KPaymentFragment.this.getActivity(),"شماره تلفن همراه اشتباه وارد شده",Toast.LENGTH_LONG).show();
                }else {
                    generatedUUD = UUID.randomUUID().toString();
                    sendServerRequest();
                }
            }
        });

        return fView;
    }

    public void setValues() {

        Kaffare kaffare = G.getInstance().kaffareToString(getActivity(), G.getInstance().getKaffare());

        kaffare_type_Txt.setText(kaffare.getKaffareType());
        days_count_txt.setText(kaffare.getDaysCount());
        cost_per_day_txt.setText(kaffare.getPrice());
        institute_txt.setText(kaffare.getInstitute());
        city_txt.setText(kaffare.getProvince()+" ," + kaffare.getCity());

        totalCostTxt.setText(G.getInstance().getKaffare().getPrice()* G.getInstance().getKaffare().getDaysCount()+" تومان ");

    }

    private void sendServerRequest() {
        JSONObject object = new JSONObject();
        ServerKaffare kaffare = G.getInstance().getKaffare();
//
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

        try {
            if(kaffare.getKaffareType() == 0) {
                nameValuePairs.add(new NameValuePair("type", "A"));
            }else{
                nameValuePairs.add(new NameValuePair("type", "O"));
            }

            nameValuePairs.add(new NameValuePair("number", kaffare.getDaysCount()+""));
            nameValuePairs.add(new NameValuePair("fee", kaffare.getPrice()+""));
            nameValuePairs.add(new NameValuePair("province", kaffare.getProvince()+""));
            nameValuePairs.add(new NameValuePair("city", kaffare.getCity()+""));
            nameValuePairs.add(new NameValuePair("institute", convertToServerIns(kaffare.getInstitute())+""));
            nameValuePairs.add(new NameValuePair("UUID",generatedUUD));
            nameValuePairs.add(new NameValuePair("signature", SignController.SignString(generatedUUD)));

        } catch (Exception e) {
            e.printStackTrace();
        }




        String url = G.BASE_URL + "kaffare_settings";

        AsyncTask<String, String, HttpResult> postService = NetworkHelper.sendPostRequest(getActivity(), url, nameValuePairs, new ServiceCallback() {
            @Override
            public void onTaskComplete(HttpResult result) {
                Log.d("ser ans",result.getContent());
                if((result.getStatus() == 200) && result.getContent().length()>0){
                    try {
                        serverAns = result.getContent();
                        int k = Integer.valueOf(serverAns);

                        Bundle bundle = new Bundle();
                        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "kaffare_payment");
                        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "click_before_payment--"+k);
                        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "text");
                        ChooseServiceActivity.mFirebaseAnalytics.logEvent("payment_start", bundle);

                        purchase();
                    }catch (Exception e){
                        Toast.makeText(getActivity(),"خطا در ارسال اطلاعات",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getActivity(),"اشکال در ارتباط با سرور",Toast.LENGTH_LONG).show();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    //////////////Fanap Payment

    KPaymentFragment.ResponseHandler responseHandler;
    ServiceConnection serviceConnection;
    Messenger serviceMessenger;

    private void initMPGCall(){

        responseHandler = new KPaymentFragment.ResponseHandler();
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                serviceMessenger = new Messenger(iBinder);
//                mBound = true;
            }
            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                serviceMessenger = null;
//                mBound = false;
            }
        };
        KPaymentFragment.this.getActivity().bindService(new Intent(KPaymentFragment.this.getActivity(), FanapMpgService.class), serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private String generateSignedString(FanapRequestPay data){
//        FanapRequestPay fanapRequestPay = new FanapRequestPay();
//        fanapRequestPay.setData(data);
        String signString = "" + data.getMerchantId() + "|" +
                data.getTerminalId() + "|" + data.getUserMobile() +
                "|" + data.getAmount() + "|"  + data.getUuid();
        String modulusElem = "", dElem = "";
        byte[] modulusBytes = null, dBytes = null;
        BigInteger modulus = null, d = null;

        modulusElem = "2/fQQxCERv36KJCft3bMcUwy05Tjv+AxyYtaw3gtTuMc7nmZbCKuAlfzXB1iGvXiJdwyiwmixq3VvyTjqyCv1FUOFSjuMP3xBmtgiyVh5VDXFa4ZLPs2IkmzQUJX5KAbhRq7eCD/LXqKB6cZilXqnMCJQvQjUqkHE3uCO50PDEM=";
        dElem = "rhifLtU9vGp7uettXemauflTKP7omw64mQnAloDha1R5q412jo2MJ7KwnDFtWmVNziVPo5fGy3T+fcCWqPS/89ezjosgsPjYyPD9qwUJ6NyjzNKLPgB1ZYbV6lV0ZzI3RMZ/vJfkJMMpo2kQGF9SsCRsWbBrELM4lY5F6Jsse+E=";
        Base64 base64 = new Base64();
        modulusBytes = base64.decodeBase64(modulusElem.getBytes());
        dBytes = base64.decodeBase64(dElem.getBytes());
        modulus = new BigInteger(1, modulusBytes);
        d = new BigInteger(1, dBytes);
        String sign = sign(signString, modulus, d);
        return sign;
    }

    public String sign(String data, BigInteger modulus, BigInteger d) {
        try {
            Signature signature = Signature.getInstance("SHA1withRSA");
            KeyFactory factory = KeyFactory.getInstance("RSA");
            RSAPrivateKeySpec privateKeySpec = new RSAPrivateKeySpec(modulus, d);
            PrivateKey privateKey = factory.generatePrivate(privateKeySpec);
            signature.initSign(privateKey); signature.update(data.getBytes("UTF-8"));
            byte[] signedByteData = signature.sign();
            Base64 base64 = new Base64();
            return new String (base64.encode(signedByteData));
        } catch (Exception ex) {
            ex.printStackTrace();
            return ""; }
    }

    class ResponseHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            int respCode = msg.what;
            switch (respCode) {
                case FanapMpgService.CBAZAAR_RESPONSE_PAYMENT: {
                    String result = msg.getData().getString(FanapMpgService.RESPONSE_DATA);
                    FanapResponsePay response = new Gson().fromJson(result,
                            FanapResponsePay.class);
                    String resp = response.getMessage();

                    Bundle bundle = new Bundle();
                    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "kaffare_payment");
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "payment_done___"+resp);
                    bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "text");
                    ChooseServiceActivity.mFirebaseAnalytics.logEvent("payment_done", bundle);

                    sendDeliveryRequest(response,serverAns);



//                    textViewAmountRes.setText(response.getAmount());
//                    textViewMessageRes.setText(response.getMessage());
//                    textViewRefNumberRes.setText(response.getReferenceNumber());
//                    textViewResultCodeRes.setText(response.getResCode());
//                    textViewUserIdRes.setText(response.getUserId());
//                    textViewUuid.setText(response.getUuid());
                } default:
            }
            super.handleMessage(msg);
        }
    }

    private void sendDeliveryRequest(final FanapResponsePay response, String serverAns) {

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

        try {

            String responseText = "Amount-"+response.getAmount()+"_Merchant-"+response.getMerchant()+"_Message-"+response.getMessage()+
                    "_Pan-"+response.getPan()+"_RefNum-"+response.getReferenceNumber()+"_Terminal-"+response.getTerminal()+
                    "_TransDate-"+response.getTransDate()+"_UserId-"+response.getUserId()+"_UserMobile-"+response.getUserMobile()+
                    "_UUID-"+response.getUuid();
            nameValuePairs.add(new NameValuePair("id", serverAns));
            nameValuePairs.add(new NameValuePair("response_code", response.getResCode()));
            nameValuePairs.add(new NameValuePair("response_text", responseText));
            nameValuePairs.add(new NameValuePair("signature", SignController.SignString(serverAns)));



        } catch (Exception e) {
            e.printStackTrace();
        }




        String url = G.BASE_URL + "payment_delivery";

        AsyncTask<String, String, HttpResult> postService = NetworkHelper.sendPostRequest(getActivity(), url, nameValuePairs, new ServiceCallback() {
            @Override
            public void onTaskComplete(HttpResult result) {
                Log.d("ser Ansdelivery", result.getContent());
                if(result.getStatus() == 200) {
                    Gson gson = new Gson();
                    String param = gson.toJson(response);

                    ((KaffareActivity)getActivity()).setFactor(response);
                    ((KaffareActivity)getActivity()).gotoNextPage();


                }else{

                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }
    private int convertToServerIns(int institude) {
        if(institude == 0){
            return 3;
        }else if(institude == 1){
            return 1;
        }else{
            return 0;
        }

    }

    void purchase()
    {
        Message msg = Message.obtain(null, FanapMpgService.WHAT_CBAZAAR_REQUEST_PAYMENT);

        msg.replyTo = mMessenger;
        Bundle bundle = new Bundle();
//        FanapRequestPay fanapRequestPay = new FanapRequestPay();
        //>>>>>> FILL fanapRequestPay fields here...
        FanapRequestPay frp = new FanapRequestPay();
        frp.setAmount(""+(G.getInstance().getKaffare().getPrice()*G.getInstance().getKaffare().getDaysCount()*10));
        frp.setCompany(company);
        frp.setMerchantId(merchantId);
        frp.setTerminalId(terminalId);
        frp.setUserMobile(""+userPhone/*"09359970247"*/);
        frp.setUuid(generatedUUD);
        frp.setSign(generateSignedString(frp));


        String reqData = new Gson().toJson(frp);
        bundle.putString(FanapMpgService.REQUEST_DATA, reqData);
        bundle.putString(FanapMpgService.SUPPORT_NUMBER, ""+userPhone/*"09359970247"*/);
        bundle.putString(FanapMpgService.PACKAGE_NAME, KPaymentFragment.this.getActivity().getApplicationContext().getPackageName());
        msg.setData(bundle);
        try {
            serviceMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }

        int test = 4;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(serviceConnection != null){
            try {
                KPaymentFragment.this.getActivity().unbindService(serviceConnection);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
