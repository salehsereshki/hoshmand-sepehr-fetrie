/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sepehr.hoshmand.fetrie.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import com.sepehr.hoshmand.fetrie.R;
import com.sepehr.hoshmand.fetrie.views.Txt;

import java.util.ArrayList;


public class ProvinceAdapter extends ArrayAdapter<String> {

    private LayoutInflater mInflater;
    private ArrayList<String> provinces;
    private Context context;
    private ArrayList<String> suggestions;
    private ArrayList<String> itemsAll;

    public ProvinceAdapter(Context context, ArrayList<String> items) {
        super(context,R.layout.province_item);
        mInflater = LayoutInflater.from(context);
        provinces = items;
        this.itemsAll = (ArrayList<String>) items.clone();
        this.suggestions = new ArrayList<String>();
        this.context = context;
    }

    @Override
    public int getCount() {
        return provinces.size();
    }


    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        final ProvinceHolder holder;

        if (convertView == null) {
            convertView = View.inflate(context,R.layout.province_item, null);
            holder = new ProvinceHolder(convertView);

            holder.titleTxt = (Txt) convertView.findViewById(R.id.item_txtvu);

            convertView.setTag(holder);
        }else{
            holder = (ProvinceHolder) convertView.getTag();
        }

        holder.titleTxt.setText(provinces.get(i));

        return convertView;
    }

    public class ProvinceHolder {
        public final View mView;
        public Txt titleTxt;

        public ProvinceHolder(View view) {
            mView = view;
            titleTxt = (Txt) view.findViewById(R.id.item_txtvu);
        }

    }


    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((String)(resultValue));
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (String customer : itemsAll) {
                    if(customer.toLowerCase().startsWith(constraint.toString().toLowerCase())){
                        suggestions.add(customer);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<String> filteredList = (ArrayList<String>) results.values;
            if(results != null && results.count > 0) {
                clear();
                for (String c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    };


}
