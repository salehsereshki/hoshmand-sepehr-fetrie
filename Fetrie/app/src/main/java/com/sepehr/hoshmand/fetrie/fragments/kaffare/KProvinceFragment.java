package com.sepehr.hoshmand.fetrie.fragments.kaffare;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sepehr.hoshmand.fetrie.KaffareActivity;
import com.sepehr.hoshmand.fetrie.MainActivity;
import com.sepehr.hoshmand.fetrie.R;
import com.sepehr.hoshmand.fetrie.general.G;
import com.sepehr.hoshmand.fetrie.object.City;
import com.sepehr.hoshmand.fetrie.object.Province;
import com.sepehr.hoshmand.fetrie.views.AutoCompleteTxt;
import com.sepehr.hoshmand.fetrie.views.Fbtn;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class KProvinceFragment extends Fragment {


    private AutoCompleteTxt province_actv;
    private AutoCompleteTxt city_actv;
    private Fbtn submitBtn;

    public KProvinceFragment() {
        // Required empty public constructor
    }
    public static KProvinceFragment newInstance() {
        KProvinceFragment fragment = new KProvinceFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fView = inflater.inflate(R.layout.fragment_province, container, false);

        province_actv = (AutoCompleteTxt) fView.findViewById(R.id.autoCompleteTextView1);
        city_actv = (AutoCompleteTxt) fView.findViewById(R.id.autoCompleteTextView2);

        province_actv.setText("تهران");
        city_actv.setText("تهران");
        submitBtn = (Fbtn) fView.findViewById(R.id.submit_btn);
        String[] countries = getResources().getStringArray(R.array.provinces);
        List<String> list = Arrays.asList(countries);
        final ArrayList<String> listOfStrings = getProvinceList();
        //listOfStrings.addAll(list);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),R.layout.province_item,listOfStrings);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ShowToast")
            @Override
            public void onClick(View view) {
                if(getIndexOfProvince(province_actv.getText()+"")!= -1) {
                    ((KaffareActivity) getActivity()).gotoNextPage();
                    G.getInstance().getKaffare().setProvince(province_actv.getText() + "");
                    G.getInstance().getKaffare().setCity(city_actv.getText() + "");
                }else{
                    Toast.makeText(getActivity(),getActivity().getResources().getString(R.string.wrong_input),Toast.LENGTH_LONG).show();
                }
            }
        });

        province_actv.setAdapter(adapter);
        province_actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                city_actv.setEnabled(true);
                city_actv.setText("");
                ArrayList<String> listOfcities = getCityList(province_actv.getText()+"");
                ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(getActivity(),R.layout.province_item,listOfcities);
                city_actv.setAdapter(cityAdapter);
            }
        });
        ArrayList<String> listOfcities = getCityList(province_actv.getText()+"");
        ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(getActivity(),R.layout.province_item,listOfcities);
        city_actv.setAdapter(cityAdapter);
        return  fView;
    }

    private ArrayList<String> getCityList(String s) {
        ArrayList<City> cities = new ArrayList<>();
        ArrayList<String> cityNames = new ArrayList<>();
        Type typeToken = new TypeToken<List<Province>>(){}.getType();
        Gson gson = new Gson();

        List<Province> provinceList = gson.fromJson(loadJSONFromAsset(),typeToken);

        for (int i = 0; i < getProvinceList().size(); i++) {
            if (provinceList.get(i).getName().equals(s)){
                cities = provinceList.get(i).getCities();
            }
        }

        for (int i = 0; i <cities.size() ; i++) {
            cityNames.add(cities.get(i).getName());
        }
        return cityNames;
    }

    private ArrayList<String> getProvinceList() {
        ArrayList<String> provinces = new ArrayList<>();
        Type typeToken = new TypeToken<List<Province>>(){}.getType();
        Gson gson = new Gson();

        List<Province> provinceList = gson.fromJson(loadJSONFromAsset(),typeToken);

        for (int i = 0; i < provinceList.size(); i++) {
            provinces.add(provinceList.get(i).getName());
        }

        return provinces;
    }

    private int getIndexOfProvince(String name){
        int index = -1;

        for (int i=0;i<getProvinceList().size();i++) {
            if (getProvinceList().get(i).equals(name)) {
                index = i;
                break;
            }
        }

        return index;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("Province.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            json.replace("\r" ,"");
            json = json.replace("\r\n" ,"");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }



}
