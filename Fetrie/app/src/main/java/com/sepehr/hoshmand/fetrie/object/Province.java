package com.sepehr.hoshmand.fetrie.object;

import java.util.ArrayList;

/**
 * Created by saliii on 6/14/17.
 */
public class Province {

    private String name;
    private ArrayList<City> Cities;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<City> getCities() {
        return Cities;
    }

    public void setCities(ArrayList<City> cities) {
        this.Cities = cities;
    }
}
