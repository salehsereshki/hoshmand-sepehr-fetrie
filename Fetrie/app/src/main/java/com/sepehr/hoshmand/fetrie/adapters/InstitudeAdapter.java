package com.sepehr.hoshmand.fetrie.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.sepehr.hoshmand.fetrie.R;
import com.sepehr.hoshmand.fetrie.general.G;
import com.sepehr.hoshmand.fetrie.object.Institude;
import com.sepehr.hoshmand.fetrie.views.Txt;
import com.squareup.picasso.Picasso;

import java.util.List;

public class InstitudeAdapter extends BaseAdapter {

    private Context context;
    private List<Institude> institudes;

    public InstitudeAdapter(Context context, List<Institude> institudes) {
        this.context = context;
        this.institudes = institudes;
    }

    public int getCount() {
        return institudes.size();
    }

    @Override
    public Object getItem(int i) {
        return institudes.get(i);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final AyyatollahViewHolder holder;

        if (convertView == null) {
            convertView = View.inflate(context,R.layout.ayyatollah_item, null);
            holder = new AyyatollahViewHolder();

            holder.institudeImgvu = (ImageView) convertView.findViewById(R.id.item_imgvu);
            holder.institudeTxtvu = (Txt) convertView.findViewById(R.id.item_txtvu);

            convertView.setTag(holder);
        }else{
            holder = (AyyatollahViewHolder) convertView.getTag();
        }




        holder.institudeTxtvu.setText(institudes.get(position).getName() + "");
        //holder.institudeImgvu.setBackgroundResource(institudes.get(position).getImageId());
        Picasso.with(context).load(institudes.get(position).getImageId()).into(holder.institudeImgvu);


        convertView.setMinimumHeight((int) (G.getInstance().getScreenProperties(context).getScreenwidthPX() / 2));
        convertView.setMinimumWidth(
                (int) (G.getInstance().getScreenProperties(context).getScreenwidthPX() / 2));

        holder.institudeImgvu.getLayoutParams().height = (int) (G.getInstance().getScreenProperties(context).getScreenwidthPX() / 2 -48);

        return convertView;
    }


    public class AyyatollahViewHolder {

        public AyyatollahViewHolder() {
        }

        Txt institudeTxtvu;
        ImageView institudeImgvu;

    }
}
