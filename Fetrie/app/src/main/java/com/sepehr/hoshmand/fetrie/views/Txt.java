package com.sepehr.hoshmand.fetrie.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.sepehr.hoshmand.fetrie.R;


public class Txt extends TextView {
    public static Typeface FONT_NAME;
    private String fontName = "BYekan.ttf";

    public Txt(Context context) {
        super(context);
        if (FONT_NAME == null)
            FONT_NAME = Typeface.createFromAsset(context.getAssets(), "fonts/isans.ttf");
        this.setTypeface(FONT_NAME);
    }

    public Txt(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.Txt, 0, 0);
        int fontFamily = a.getInteger(R.styleable.Txt_fontfamily, 0);
        if (fontFamily == 2)
            fontName = "isans.ttf";
        else
            fontName = "isans.ttf";


        if (FONT_NAME == null)
            FONT_NAME = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontName);
        this.setTypeface(FONT_NAME);
    }

    public Txt(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.Txt, 0, 0);
        int fontFamily = a.getInteger(R.styleable.Txt_fontfamily, 0);
        if (fontFamily == 2)
            fontName = "BYekan.ttf";
        else
            fontName = "BYekan.ttf";


        if (FONT_NAME == null)
            FONT_NAME = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontName);
        this.setTypeface(FONT_NAME);
    }
}