package com.sepehr.hoshmand.fetrie.fragments.kaffare;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.sepehr.hoshmand.fetrie.KaffareActivity;
import com.sepehr.hoshmand.fetrie.MainActivity;
import com.sepehr.hoshmand.fetrie.R;
import com.sepehr.hoshmand.fetrie.adapters.InstitudeAdapter;
import com.sepehr.hoshmand.fetrie.general.G;
import com.sepehr.hoshmand.fetrie.object.Institude;

import java.util.ArrayList;
import java.util.List;


public class KInstitudeFragment extends Fragment {

    private GridView gridView;

    public KInstitudeFragment() {
        // Required empty public constructor
    }
    public static KInstitudeFragment newInstance() {
        KInstitudeFragment fragment = new KInstitudeFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fView = inflater.inflate(R.layout.fragment_kaffare_institude, container, false);

        gridView = (GridView) fView.findViewById(R.id.grid);

        gridView.setAdapter(new InstitudeAdapter(getActivity(),getInstitudes()));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                G.getInstance().getKaffare().setInstitute(i);
                ((KaffareActivity)getActivity()).updatePaymentFragment();
                ((KaffareActivity)getActivity()).gotoNextPage();
            }
        });


        return  fView;
    }


    private List<Institude> getInstitudes() {

        List<Institude> institudes = new ArrayList<>();

        TypedArray typedArray = getResources().obtainTypedArray(R.array.institude_images);

        for (int i = 0; i < getResources().getStringArray(R.array.institudes).length; i++) {
            Institude institude = new Institude();

            institude.setName(getResources().getStringArray(R.array.institudes)[i]);
            institude.setImageId(typedArray.getResourceId(i, R.drawable.yes));
            institudes.add(institude);
        }

        return institudes;
    }

}
