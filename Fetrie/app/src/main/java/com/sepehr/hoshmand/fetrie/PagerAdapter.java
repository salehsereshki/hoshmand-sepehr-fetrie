package com.sepehr.hoshmand.fetrie;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sepehr.hoshmand.fetrie.fragments.FactorFragment;
import com.sepehr.hoshmand.fetrie.fragments.InstitudeFragment;
import com.sepehr.hoshmand.fetrie.fragments.PayToFragment;
import com.sepehr.hoshmand.fetrie.fragments.PaymentFragment;
import com.sepehr.hoshmand.fetrie.fragments.PersonsCountFragment;
import com.sepehr.hoshmand.fetrie.fragments.PriceFragment;
import com.sepehr.hoshmand.fetrie.fragments.ProvinceFragment;
import com.sepehr.hoshmand.fetrie.fragments.StartFragment;
import com.sepehr.hoshmand.fetrie.fragments.SyadatFragment;
import com.sepehr.hoshmand.fetrie.fragments.TypeFragment;

import java.util.HashMap;

public class PagerAdapter extends FragmentPagerAdapter {


    private Context context;

    HashMap<Integer,Fragment> fragments;

    private UpdateFragment updateFragment;

    public PagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        fragments = new HashMap<>();
    }

    @Override
    public Fragment getItem(int position) {

        if(position == 9) {
            Fragment f = StartFragment.newInstance();
            fragments.put(9,f);
            return f;
        }
        if(position == 8) {
            Fragment f = TypeFragment.newInstance();
            fragments.put(8,f);
            return f;
        }if(position==7){
            Fragment f = SyadatFragment.newInstance();
            fragments.put(7,f);
            return f;
        }if(position==6){
            Fragment f = PayToFragment.newInstance();
            fragments.put(6,f);
            return f;
        }if(position==5){
            Fragment f = PriceFragment.newInstance();
            fragments.put(5,f);
            return f;
        }if(position==4){
            Fragment f = ProvinceFragment.newInstance();
            fragments.put(4,f);
            return f;
        }if(position==3){
            Fragment f = PersonsCountFragment.newInstance();
            fragments.put(3,f);
            return f;
        }if(position==2){
            Fragment f = InstitudeFragment.newInstance();
            fragments.put(2,f);
            return f;
        }if(position==1){
            Fragment f = PaymentFragment.newInstance();
            fragments.put(1,f);
            return f;
        }if(position==0){
            Fragment f = FactorFragment.newInstance();
            fragments.put(0,f);
            return f;
        }else{
            Fragment f = TypeFragment.newInstance();
            fragments.put(-1,f);
            return f;
        }
    }

    public UpdateFragment getUpdateFragment() {
        return updateFragment;
    }

    public void setUpdateFragment(UpdateFragment updateFragment) {
        this.updateFragment = updateFragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return context.getResources().getStringArray(R.array.titles)[position];
    }

    @Override
    public int getCount() {
        return 10;
    }

    public Fragment getItemWithPosition(int position) {
        return fragments.get(position);
    }
}