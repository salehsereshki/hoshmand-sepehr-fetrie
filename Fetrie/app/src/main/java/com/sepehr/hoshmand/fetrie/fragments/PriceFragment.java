package com.sepehr.hoshmand.fetrie.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.sepehr.hoshmand.fetrie.MainActivity;
import com.sepehr.hoshmand.fetrie.R;
import com.sepehr.hoshmand.fetrie.general.G;
import com.sepehr.hoshmand.fetrie.views.EditTxt;
import com.sepehr.hoshmand.fetrie.views.Fbtn;


public class PriceFragment extends Fragment {

    private EditTxt priceEdtTxt;
    private Fbtn submitBtn;

    public PriceFragment() {
        // Required empty public constructor
    }
    public static PriceFragment newInstance() {
        PriceFragment fragment = new PriceFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fView = inflater.inflate(R.layout.fragment_price, container, false);

        priceEdtTxt = (EditTxt) fView.findViewById(R.id.autoCompleteTextView1);
        submitBtn = (Fbtn) fView.findViewById(R.id.submit_btn);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isValidNumber(priceEdtTxt.getText()+"")){
                    ((MainActivity) getActivity()).gotoNextPage();
                    G.getInstance().getFetrie().setCost(Integer.parseInt(priceEdtTxt.getText()+""));
                }else{
                    Toast.makeText(getActivity(),getActivity().getResources().getString(R.string.wrong_input),Toast.LENGTH_LONG).show();
                }
            }
        });

        if(priceEdtTxt.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }


        return  fView;
    }

    private boolean isValidNumber(String text) {

        try {
            int num = Integer.parseInt(text);
            Log.i("",num+" is a number");

            if(text.length()==0){
                return false;
            }
            return true;
        } catch (NumberFormatException e) {
            Log.i("",text+" is not a number");
            return false;
        }
    }


}
