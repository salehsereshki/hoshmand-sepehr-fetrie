package com.sepehr.hoshmand.fetrie.object;

/**
 * Created by saliii on 6/14/17.
 */
public class Kaffare {

    private String kaffareType;
    private String price;
    private String daysCount;
    private String institute;
    private String province;
    private String city;

    public String getKaffareType() {
        return kaffareType;
    }

    public void setKaffareType(String kaffareType) {
        this.kaffareType = kaffareType;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDaysCount() {
        return daysCount;
    }

    public void setDaysCount(String daysCount) {
        this.daysCount = daysCount;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
}
