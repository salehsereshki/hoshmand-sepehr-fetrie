package com.sepehr.hoshmand.fetrie.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sepehr.hoshmand.fetrie.MainActivity;
import com.sepehr.hoshmand.fetrie.general.G;
import com.sepehr.hoshmand.fetrie.views.Txt;
import com.sepehr.hoshmand.fetrie.R;


public class TypeFragment extends Fragment {

    LinearLayout wheatLay;
    LinearLayout riceLay;
    ImageView wheatImgvu;
    Txt wheatTxtvu;
    ImageView riceImgvu;
    Txt riceTxtvu;



    public TypeFragment() {
        // Required empty public constructor
    }
    public static TypeFragment newInstance() {
        TypeFragment fragment = new TypeFragment();
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fView = inflater.inflate(R.layout.fragment_type, container, false);


        wheatLay = (LinearLayout) fView.findViewById(R.id.wheet_lay);
        riceLay = (LinearLayout) fView.findViewById(R.id.rice_lay);


        wheatImgvu = (ImageView) wheatLay.findViewById(R.id.item_imgvu);
        wheatTxtvu = (Txt) wheatLay.findViewById(R.id.item_txtvu);


        riceImgvu = (ImageView) riceLay.findViewById(R.id.item_imgvu);
        riceTxtvu = (Txt) riceLay.findViewById(R.id.item_txtvu);

        wheatImgvu.setBackgroundResource(R.drawable.wheat);
        wheatTxtvu.setText(getResources().getString(R.string.wheat));

        riceImgvu.setBackgroundResource(R.drawable.rice);
        riceTxtvu.setText(getResources().getString(R.string.rice));

        wheatLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).gotoNextPage();
                G.getInstance().getFetrie().setFoodType(0);
            }
        });

        riceLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).gotoNextPage();
                G.getInstance().getFetrie().setFoodType(1);
            }
        });


        return fView;
    }
}
