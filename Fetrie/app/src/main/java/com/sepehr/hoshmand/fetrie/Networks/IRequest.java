package com.sepehr.hoshmand.fetrie.Networks;


/**
 * Created by saliii on 4/21/17.
 */
public interface IRequest {

    public void onPreRequest();
    public void onRequestSend();
    public void onSuccessRequest(Entity entity);
    public void onFailedRequest();


}
