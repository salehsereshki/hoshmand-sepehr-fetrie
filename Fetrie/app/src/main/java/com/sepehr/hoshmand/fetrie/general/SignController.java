package com.sepehr.hoshmand.fetrie.general;

import android.util.Base64;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.StringReader;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.UUID;

import javax.crypto.Cipher;

/**
 * Created by sina on 6/15/2017 AD.
 */

public class SignController {

    public final static String PRIVATE_KEY =
    "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIICWwIBAAKBgGpelgd+EO36cKxL7mkdSM7053UToWSQ9sVY2AH6oaXeB8IFf2ze\n" +
            "9+0/z/RmDNCGOXpC5Hb4KhoGPZA5vPbCJLZjzqAQmR2TWVLmXsv0CnLze1x10J9N\n" +
            "1fezIRft+1PiAZ5WeLWJRUCwkOWAGaPgwFKw6z/GaG3eNmVCod2bXaB3AgMBAAEC\n" +
            "gYAXT2cdeTkIjeuHNpSmoG5GPFFEzOp/eW06zj94H46xJLkagc3s8+ftLCDMdFi2\n" +
            "kdOiwWoD0FqZnWh8ZMrOYLlenSn1iWPxwSR2cd+fJbOvfYfjAJaAQujnjyTC0iNh\n" +
            "LP4j2SHYXsqSQq4IDBjApLPDJ2hRmhpP+jELtuF0cqBdwQJBAMovr/K7IfIci0g7\n" +
            "u5A3qcTozqbW4jwEGK0/pDZlkcSeAJ7VNLhTwEYrj+bWgVUCqq/fDVo/hIqtJLpV\n" +
            "haVgWxECQQCGrkE7LiyX0nG7VTRxmt2xxjNkf7r6Dm556h2SU9q010i92OEqcVus\n" +
            "l40+1yy3OeXV2YVWBd3NcIxuidDGqvMHAkEApL7wtm9m/JnnoAcUETJaAhnU8Ttd\n" +
            "Apwv41NdIfnnKfe497MwjxWnMUYjrmuQ3M0cecvdXHEvNbRZnf87tAJskQJAH/D9\n" +
            "6YI28z7m2E4J5ROclofyMliUyK9ZhjJ/FvAqQ493Ygcox78gxdsflZLEuSPTHZe8\n" +
            "Jr23b/zMYX7v894hSwJAJhG2su/DpQpPpT33wac4HDRHhNYddkqnzzEiYePDxGad\n" +
            "5I7dcjZZKqb/wWdxlxVltfc776VdDwJqoGnLXVHAiw==\n" +
            "-----END RSA PRIVATE KEY-----";

    SignController(){

    }
    public static String SignString(String generatedUUD) {
        String signature = "";
        try {

            StringBuilder pkcs8Lines = new StringBuilder();
            BufferedReader rdr = new BufferedReader(new StringReader(SignController.PRIVATE_KEY));
            String line;
            while ((line = rdr.readLine()) != null) {
                pkcs8Lines.append(line);
            }

            // Remove the "BEGIN" and "END" lines, as well as any whitespace

            String pkcs8Pem = pkcs8Lines.toString();
            pkcs8Pem = pkcs8Pem.replace("-----BEGIN PRIVATE KEY-----", "");
            pkcs8Pem = pkcs8Pem.replace("-----END PRIVATE KEY-----", "");
            pkcs8Pem = pkcs8Pem.replace("-----BEGIN RSA PRIVATE KEY-----", "");
            pkcs8Pem = pkcs8Pem.replace("-----END RSA PRIVATE KEY-----", "");
            pkcs8Pem = pkcs8Pem.replaceAll("\\s+", "");

            // Base64 decode the result

            org.apache.commons.codec.binary.Base64 b64 = new org.apache.commons.codec.binary.Base64();
            byte[] pkcs8EncodedBytes = b64.decode(pkcs8Pem.getBytes());

            // extract the private key

            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(pkcs8EncodedBytes);
            KeyFactory kf = KeyFactory.getInstance("RSA", "BC");
            PrivateKey privKey = kf.generatePrivate(keySpec);


            Signature privateSignature = Signature.getInstance("SHA256withRSA");
            privateSignature.initSign(privKey);
            privateSignature.update(generatedUUD.getBytes("utf-8"));

            byte[] signatureb = privateSignature.sign();

            signature = android.util.Base64.encodeToString(signatureb, android.util.Base64.DEFAULT);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return signature;
    }
}
