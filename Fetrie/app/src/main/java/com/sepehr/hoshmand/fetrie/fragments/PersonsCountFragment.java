package com.sepehr.hoshmand.fetrie.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sepehr.hoshmand.fetrie.MainActivity;
import com.sepehr.hoshmand.fetrie.R;
import com.sepehr.hoshmand.fetrie.general.G;
import com.sepehr.hoshmand.fetrie.views.EditTxt;
import com.sepehr.hoshmand.fetrie.views.Fbtn;


public class PersonsCountFragment extends Fragment {


    private EditTxt personCountTxt;
    private Fbtn submitBtn;

    public PersonsCountFragment() {
        // Required empty public constructor
    }
    public static PersonsCountFragment newInstance() {
        PersonsCountFragment fragment = new PersonsCountFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fView = inflater.inflate(R.layout.fragment_person_count, container, false);

        personCountTxt = (EditTxt) fView.findViewById(R.id.person_count_edttxt);
        submitBtn = (Fbtn) fView.findViewById(R.id.submit_btn);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(isValidNumber(personCountTxt.getText()+"")){
                    G.getInstance().getFetrie().setPersonNum(Integer.parseInt(personCountTxt.getText()+""));
                    ((MainActivity)getActivity()).gotoNextPage();
                }else{
                    Toast.makeText(getActivity(),getActivity().getResources().getString(R.string.wrong_input),Toast.LENGTH_LONG).show();
                }
            }
        });

        return  fView;
    }

    private boolean isValidNumber(String text) {

        try {
            int num = Integer.parseInt(text);
            Log.i("",num+" is a number");

            if(text.length()==0){
                return false;
            }
            return true;
        } catch (NumberFormatException e) {
            Log.i("",text+" is not a number");
            return false;
        }
    }
}
