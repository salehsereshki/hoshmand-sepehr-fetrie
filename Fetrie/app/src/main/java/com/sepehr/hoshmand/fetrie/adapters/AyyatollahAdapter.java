package com.sepehr.hoshmand.fetrie.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.sepehr.hoshmand.fetrie.general.G;
import com.sepehr.hoshmand.fetrie.object.Ayyatollah;
import com.sepehr.hoshmand.fetrie.views.Txt;
import com.sepehr.hoshmand.fetrie.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AyyatollahAdapter extends BaseAdapter {

    private Context context;
    private List<Ayyatollah> ayyatollahs;

    public AyyatollahAdapter(Context context, List<Ayyatollah> ayyatollahs) {
        this.context = context;
        this.ayyatollahs = ayyatollahs;
    }

    public int getCount() {
        return ayyatollahs.size();
    }

    @Override
    public Object getItem(int i) {
        return ayyatollahs.get(i);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final AyyatollahViewHolder holder;

        if (convertView == null) {
            convertView = View.inflate(context, R.layout.ayyatollah_item, null);
            holder = new AyyatollahViewHolder();

            holder.ayytImgvu = (ImageView) convertView.findViewById(R.id.item_imgvu);
            holder.ayyatTxtvu = (Txt) convertView.findViewById(R.id.item_txtvu);
            holder.ayyatPriceTxtvu = (Txt) convertView.findViewById(R.id.item_price_txtvu);

            convertView.setTag(holder);
        }else{
            holder = (AyyatollahViewHolder) convertView.getTag();
        }




        holder.ayyatTxtvu.setText(ayyatollahs.get(position).getName() + "");
        holder.ayyatPriceTxtvu.setText(ayyatollahs.get(position).getPrice() + "");
        //holder.institudeImgvu.setBackgroundResource(ayyatollahs.get(position).getImageId());
        Picasso.with(context).load(ayyatollahs.get(position).getImageId()).into(holder.ayytImgvu);


        convertView.setMinimumHeight((int) (G.getInstance().getScreenProperties(context).getScreenwidthPX() / 2));
        convertView.setMinimumWidth(
                (int) (G.getInstance().getScreenProperties(context).getScreenwidthPX() / 2));

        holder.ayytImgvu.getLayoutParams().height = (int) (G.getInstance().getScreenProperties(context).getScreenwidthPX() / 2 -48);

        return convertView;
    }


    public class AyyatollahViewHolder {

        public AyyatollahViewHolder() {
        }

        Txt ayyatTxtvu;
        Txt ayyatPriceTxtvu;
        ImageView ayytImgvu;

    }
}
