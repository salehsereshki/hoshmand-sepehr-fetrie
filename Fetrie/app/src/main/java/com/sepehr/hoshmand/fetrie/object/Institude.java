package com.sepehr.hoshmand.fetrie.object;

/**
 * Created by saliii on 5/29/17.
 */
public class Institude {
    private int imageId;
    private String name;

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

