package com.sepehr.hoshmand.fetrie.Networks;


/**
 * Created by saliii on 4/21/17.
 */
public interface ISignin {

    public void onPreRequest();
    public void onRequestSend();
    public void onSuccessSignin();

}
