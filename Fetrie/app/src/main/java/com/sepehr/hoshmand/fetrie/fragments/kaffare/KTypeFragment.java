package com.sepehr.hoshmand.fetrie.fragments.kaffare;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sepehr.hoshmand.fetrie.KaffareActivity;
import com.sepehr.hoshmand.fetrie.R;
import com.sepehr.hoshmand.fetrie.general.G;


public class KTypeFragment extends Fragment {


    private CardView amd_crdvu;
    private CardView ozr_crdvu;


    public KTypeFragment() {
        // Required empty public constructor
    }
    public static KTypeFragment newInstance() {
        KTypeFragment fragment = new KTypeFragment();
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fView = inflater.inflate(R.layout.fragment_kaffare_type, container, false);
        amd_crdvu = (CardView) fView.findViewById(R.id.amd_crdvu);
        ozr_crdvu = (CardView) fView.findViewById(R.id.ozr_crdvu);

        amd_crdvu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                G.getInstance().getKaffare().setKaffareType(0);
                ((KaffareActivity)getActivity()).gotoNextPage();
            }
        });

        ozr_crdvu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                G.getInstance().getKaffare().setKaffareType(1);
                ((KaffareActivity)getActivity()).gotoNextPage();
            }
        });

        return fView;
    }
}
