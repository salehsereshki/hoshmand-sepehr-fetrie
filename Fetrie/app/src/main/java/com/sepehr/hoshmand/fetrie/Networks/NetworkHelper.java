package com.sepehr.hoshmand.fetrie.Networks;

import android.content.Context;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class NetworkHelper {
    public static List<ReqHeader> getHeaders(Context context) {
        List<ReqHeader> reqHeaders = new ArrayList<>();
//        try {
//            if(G.getInstance().getDBStore(Token.class, context).getCount() >1){
//                G.getInstance().getDBStore(Token.class, context).deleteAll();
//                ((Activity)context).finish();
//                Intent intent = new Intent(context, SignInActivity.class);
//                context.startActivity(intent);
//            }else {
//                ReqHeader reqHeader = new ReqHeader("token", G.getInstance().getDBStore(Token.class, context).getUniqueOrThrow().getToken());
//                reqHeaders.add(reqHeader);
//            }
//        }catch (Exception e){
//
//        }
        return reqHeaders;
    }

    public static NetworkService sendGetRequest(Context context, String url, ServiceCallback callback) {
        return new NetworkService(url, NetworkHelper.getHeaders(context), callback);
    }

    public static NetworkService sendPostRequest(Context context, String url, JSONObject body, ServiceCallback callback) {
        return new NetworkService(url, body, HttpMethod.POST, getHeaders(context), callback);
    }



//    public static NetworkService sendPostRequest(Context context, String url, ServiceCallback callback) {
//        return sendPostRequest(context, url, null, callback);
//    }



    public static NetworkService sendPostRequest(Context context, String url, List<NameValuePair> nameValuePairs, ServiceCallback callback) {
        return new PostNVService(url, getHeaders(context), callback, nameValuePairs);
    }

}
