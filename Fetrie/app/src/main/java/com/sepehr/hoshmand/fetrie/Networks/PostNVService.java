package com.sepehr.hoshmand.fetrie.Networks;


import java.util.List;

import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;


public class PostNVService extends NetworkService {
    private List<NameValuePair> nameValuePairs;

    public PostNVService(String url, List<ReqHeader> headers, ServiceCallback callback, List<NameValuePair> nameValuePairs) {
        super(url, null, HttpMethod.POST, headers, callback);
        this.nameValuePairs = nameValuePairs;
    }

    @Override
    public Request.Builder setBody(Request.Builder builder) {
        FormBody.Builder bodyBuilder = new FormBody.Builder();
        for (int i = 0; i < nameValuePairs.size(); i++) {
            bodyBuilder.add(nameValuePairs.get(i).getName() , nameValuePairs.get(i).getValue());
        }
        RequestBody formBody = bodyBuilder.build();
        return builder.post(formBody);
    }
}