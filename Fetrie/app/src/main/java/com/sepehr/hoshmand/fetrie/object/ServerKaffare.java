package com.sepehr.hoshmand.fetrie.object;

/**
 * Created by saliii on 6/14/17.
 */
public class ServerKaffare {

    private int kaffareType;
    private int price;
    private int daysCount;
    private int institute;
    private String province;
    private String city;


    public int getKaffareType() {
        return kaffareType;
    }

    public void setKaffareType(int kaffareType) {
        this.kaffareType = kaffareType;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDaysCount() {
        return daysCount;
    }

    public void setDaysCount(int daysCount) {
        this.daysCount = daysCount;
    }

    public int getInstitute() {
        return institute;
    }

    public void setInstitute(int institute) {
        this.institute = institute;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
}
