package com.sepehr.hoshmand.fetrie.object;

/**
 * Created by saliii on 5/29/17.
 */
public class Ayyatollah {

    private int imageId;
    private String name;
    private String price;

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
