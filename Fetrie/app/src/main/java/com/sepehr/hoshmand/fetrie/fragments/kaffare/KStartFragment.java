package com.sepehr.hoshmand.fetrie.fragments.kaffare;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sepehr.hoshmand.fetrie.KaffareActivity;
import com.sepehr.hoshmand.fetrie.R;
import com.sepehr.hoshmand.fetrie.views.Fbtn;
import com.sepehr.hoshmand.fetrie.views.Txt;


public class KStartFragment extends Fragment {

    private Fbtn start_btn;
    private Txt start_desc_1;
    private Txt start_desc_2;
    private Txt start_desc_3;

    public KStartFragment() {
        // Required empty public constructor
    }
    public static KStartFragment newInstance() {
        KStartFragment fragment = new KStartFragment();
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fView = inflater.inflate(R.layout.fragment_kaffare_start, container, false);

        start_desc_1 = (Txt) fView.findViewById(R.id.start_desc_1);
        start_desc_2 = (Txt) fView.findViewById(R.id.start_desc_2);
        start_desc_3 = (Txt) fView.findViewById(R.id.start_desc_3);
        start_desc_1.setText("توضیحات");
        start_desc_2.setVisibility(View.GONE);
        start_desc_3.setVisibility(View.GONE);
        start_desc_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start_desc_1.setText("باسلام وآرزوی قبولی طاعات وعبادات شما");
                start_desc_2.setVisibility(View.VISIBLE);
                start_desc_3.setVisibility(View.VISIBLE);
            }
        });
        start_btn = (Fbtn) fView.findViewById(R.id.submit_btn);

        start_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((KaffareActivity)getActivity()).gotoNextPage();
            }
        });

        return fView;
    }
}
