package com.sepehr.hoshmand.fetrie.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sepehr.hoshmand.fetrie.MainActivity;
import com.sepehr.hoshmand.fetrie.R;
import com.sepehr.hoshmand.fetrie.general.G;
import com.sepehr.hoshmand.fetrie.views.Txt;


public class PayToFragment extends Fragment {

    LinearLayout yesLay;
    LinearLayout noLay;
    ImageView yesImgvu;
    Txt yesTxtvu;
    ImageView noImgvu;
    Txt noTxtvu;


    public PayToFragment() {
        // Required empty public constructor
    }

    public static PayToFragment newInstance() {
        PayToFragment fragment = new PayToFragment();
        return fragment;
    }

    View fView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        fView = inflater.inflate(R.layout.fragment_pay_to, container, false);

        yesLay = (LinearLayout) fView.findViewById(R.id.yes_lay);
        noLay = (LinearLayout) fView.findViewById(R.id.no_lay);


        yesImgvu = (ImageView) yesLay.findViewById(R.id.item_imgvu);
        yesTxtvu = (Txt) yesLay.findViewById(R.id.item_txtvu);

        noImgvu = (ImageView) noLay.findViewById(R.id.item_imgvu);
        noTxtvu = (Txt) noLay.findViewById(R.id.item_txtvu);

        yesImgvu.setBackgroundResource(R.drawable.yes);
        yesTxtvu.setText(getResources().getString(R.string.yes));


        noImgvu.setBackgroundResource(R.drawable.no);
        noTxtvu.setText(getResources().getString(R.string.no));

        yesLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).gotoNextPage();
                G.getInstance().getFetrie().setPaytoSeyyed(true);
            }
        });

        noLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).gotoNextPage();
                G.getInstance().getFetrie().setPaytoSeyyed(false);
            }
        });


        return fView;
    }

    public void updateView() {
        if (!G.getInstance().getFetrie().isSeyyed()) {
            yesLay = (LinearLayout) fView.findViewById(R.id.yes_lay);
            noLay = (LinearLayout) fView.findViewById(R.id.no_lay);
            noLay.setVisibility(View.GONE);
            yesTxtvu.setText(getActivity().getString(R.string.next));
            Txt title = (Txt) fView.findViewById(R.id.title_txtvu);
            title.setVisibility(View.INVISIBLE);
        }
    }
}
