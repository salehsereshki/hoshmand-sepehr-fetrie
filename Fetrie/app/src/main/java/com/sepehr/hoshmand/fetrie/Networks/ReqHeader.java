package com.sepehr.hoshmand.fetrie.Networks;

/**
 * Created by saliii on 6/29/16.
 */
public class ReqHeader {

    private String name;
    private String value;


    public ReqHeader(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
