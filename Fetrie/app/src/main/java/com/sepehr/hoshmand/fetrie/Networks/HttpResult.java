package com.sepehr.hoshmand.fetrie.Networks;

import com.google.gson.Gson;

import java.lang.reflect.Type;

/**
 * Created by saliii on 6/29/16.
 */
public class HttpResult {

    private int status;
    private String content;


    public HttpResult(int status, String content) {
        this.status = status;
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public <T> T getObject(Type typeToken){
        Gson gson = new Gson();
        T r;
        try {
//            Type typeToken = new TypeToken<T>() {
//            }.getType();
            r = gson.fromJson(content,typeToken);
        } catch (Exception e) {
            r = null;
        }
        return r;
    }
}
