package com.sepehr.hoshmand.fetrie.Networks;

public class AssertArguments {

    public static void notNull(Object value, String paramName) {
        notNull(value, paramName, null);
    }

    public static void notNull(Object value, String paramName, String message) {
        if (value == null) {
            throw new IllegalArgumentException("the parameter '" + paramName + "' can not be null." + ((message != null && !message.isEmpty()) ? (" message: " + message) : ""));
        }
    }
}
