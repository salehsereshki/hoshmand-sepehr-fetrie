package com.sepehr.hoshmand.fetrie.general;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.Display;

public class ScreenProperties {

    private Context context;

    private boolean isInitialized = false;

    private float screenHeightDP;
    private float screenwidthDP;
    private float screenHeightPX;
    private float screenwidthPX;
    private float itemSizePX = 0;
    private int columnNum = 0;

    public ScreenProperties(Context context) {
        this.context = context;
        if(!isInitialized()){
            setScreenSizes();
            setMainPageItemSize();
            isInitialized = true;
        }
    }

    public float getItemSizePX(){
        if(itemSizePX <1){
            setScreenSizes();
            setMainPageItemSize();
        }
        return itemSizePX;
    }

    public int getColumnNum(){
        if(columnNum <1){
            setScreenSizes();
            setMainPageItemSize();
        }
        return columnNum;
    }

    public  void setScreenSizes() {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

        Display display = ((Activity)context).getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = context.getResources().getDisplayMetrics().density;
        float dpHeight = outMetrics.heightPixels / density;
        float dpWidth = outMetrics.widthPixels / density;

        screenHeightPX = outMetrics.heightPixels;
        screenwidthPX = outMetrics.widthPixels;

        screenHeightDP = outMetrics.heightPixels / density;
        screenwidthDP = outMetrics.widthPixels / density;
    }

    public void setMainPageItemSize() {
        float minItemSize = 110;

        float efficient = screenwidthDP - 8;
        int itemNumber = (int) (efficient / minItemSize);
        float itemSize = efficient / itemNumber;
        itemSizePX = convertDpToPixels(context, itemSize) - 22;
        columnNum = itemNumber;

    }

    public  float convertDpToPixels(Context context, float dp) {
        if (context == null) {
            return -1;
        }
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public  float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp;
        float b = (metrics.densityDpi / 160f);
        dp = (px) / (b);
        return dp;
    }

    public float getScreenHeightDP() {
        return screenHeightDP;
    }

    public void setScreenHeightDP(float screenHeightDP) {
        this.screenHeightDP = screenHeightDP;
    }

    public float getScreenwidthDP() {
        return screenwidthDP;
    }

    public void setScreenwidthDP(float screenwidthDP) {
        this.screenwidthDP = screenwidthDP;
    }

    public float getScreenHeightPX() {
        return screenHeightPX;
    }

    public void setScreenHeightPX(float screenHeightPX) {
        this.screenHeightPX = screenHeightPX;
    }

    public float getScreenwidthPX() {
        return screenwidthPX;
    }

    public void setScreenwidthPX(float screenwidthPX) {
        this.screenwidthPX = screenwidthPX;
    }

    public void setItemSizePX(float itemSizePX) {
        this.itemSizePX = itemSizePX;
    }

    public void setColumnNum(int columnNum) {
        this.columnNum = columnNum;
    }

    public boolean isInitialized() {
        return isInitialized;
    }

    public void setIsInitialized(boolean isInitialized) {
        this.isInitialized = isInitialized;
    }
}
