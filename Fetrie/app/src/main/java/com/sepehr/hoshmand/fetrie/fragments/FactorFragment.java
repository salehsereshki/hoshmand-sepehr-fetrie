package com.sepehr.hoshmand.fetrie.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.sepehr.hoshmand.fetrie.MainActivity;
import com.sepehr.hoshmand.fetrie.R;
import com.sepehr.hoshmand.fetrie.views.Fbtn;
import com.sepehr.hoshmand.fetrie.views.Txt;

import ir.fanap.psp.fanapinapppayment.model.FanapResponsePay;


public class FactorFragment extends Fragment {


    private FanapResponsePay response;

    private Txt result_txtvu;
    private Txt amount_txtvu;
    private Txt transaction_id_txtvu;
    private Fbtn back_btn;


    public FactorFragment() {
        // Required empty public constructor
    }
    public static FactorFragment newInstance() {
        FactorFragment fragment = new FactorFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fView = inflater.inflate(R.layout.fragment_factor, container, false);

        result_txtvu = (Txt) fView.findViewById(R.id.result_txtvu);
        amount_txtvu = (Txt) fView.findViewById(R.id.amount_txt);
        transaction_id_txtvu = (Txt) fView.findViewById(R.id.transaction_num_txtvu);
        back_btn = (Fbtn) fView.findViewById(R.id.back_btn);



        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        return  fView;
    }

    public void setResponse(FanapResponsePay response) {
        this.response = response;

        setValues();
    }

    private void setValues() {

        result_txtvu.setText(response.getMessage().replace("#", ""));
        if(response.getResCode().equals("0")){
            result_txtvu.setTextColor(ContextCompat.getColor(getActivity(),R.color.light_green));
            amount_txtvu.setText(response.getAmount() + "ریال");
        }else{
            result_txtvu.setTextColor(ContextCompat.getColor(getActivity(),R.color.light_red));
        }

        transaction_id_txtvu.setText(response.getReferenceNumber());
        result_txtvu.setVisibility(View.VISIBLE);
        amount_txtvu.setVisibility(View.VISIBLE);
        back_btn.setVisibility(View.VISIBLE);
    }
}
