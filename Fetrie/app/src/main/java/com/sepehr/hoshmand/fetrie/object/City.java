package com.sepehr.hoshmand.fetrie.object;

/**
 * Created by saliii on 6/14/17.
 */
public class City {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
