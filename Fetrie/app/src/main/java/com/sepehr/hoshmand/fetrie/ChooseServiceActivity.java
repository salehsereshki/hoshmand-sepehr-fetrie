package com.sepehr.hoshmand.fetrie;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;
import com.sepehr.hoshmand.fetrie.fragments.PayToFragment;
import com.sepehr.hoshmand.fetrie.views.Fbtn;
import com.viewpagerindicator.TitlePageIndicator;

public class ChooseServiceActivity extends AppCompatActivity {

    public static FirebaseAnalytics mFirebaseAnalytics;

    private Fbtn fetrie;
    private Fbtn kaffare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_service);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        fetrie = (Fbtn) findViewById(R.id.choose_fetrie_btn);
        kaffare = (Fbtn) findViewById(R.id.choose_kaffare_btn);

        fetrie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent main = new Intent(ChooseServiceActivity.this, MainActivity.class);
                startActivity(main);
            }
        });

        kaffare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent main = new Intent(ChooseServiceActivity.this, KaffareActivity.class);
                startActivity(main);
            }
        });
    }

}
