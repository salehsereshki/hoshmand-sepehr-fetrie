package com.sepehr.hoshmand.fetrie.general;

import android.content.Context;

import com.sepehr.hoshmand.fetrie.R;
import com.sepehr.hoshmand.fetrie.object.Fetrie;
import com.sepehr.hoshmand.fetrie.object.Kaffare;
import com.sepehr.hoshmand.fetrie.object.ServerFetrie;
import com.sepehr.hoshmand.fetrie.object.ServerKaffare;

/**
 * Created by saliii on 5/29/17.
 */
public class G {
    public static String BASE_URL = /*"http://nik.bsi.ir/mobile/"*/"http://192.69.204.36/mobile/";
    private ScreenProperties screenProperties;

    public ScreenProperties getScreenProperties(Context mContext) {
        if (screenProperties == null) {
            screenProperties = new ScreenProperties(mContext);
        }
        return screenProperties;
    }

    private ServerFetrie fetrie;
    private ServerKaffare kaffare;


    public ServerKaffare getKaffare() {
        if (kaffare == null) {
            kaffare = new ServerKaffare();
        }
        return kaffare;
    }

    public ServerFetrie getFetrie() {
        if (fetrie == null) {
            fetrie = new ServerFetrie();
        }
        return fetrie;
    }

    public Kaffare kaffareToString(Context context, ServerKaffare serverKaffare) {
        Kaffare kaffare = new Kaffare();

        kaffare.setDaysCount(serverKaffare.getDaysCount() + "");
        kaffare.setInstitute(context.getResources().getStringArray(R.array.institudes)[serverKaffare.getInstitute()]);
        kaffare.setKaffareType(convertKaffareType(serverKaffare.getKaffareType()));
        kaffare.setPrice(serverKaffare.getPrice() + "");
        kaffare.setCity(serverKaffare.getCity());
        kaffare.setProvince(serverKaffare.getProvince());

        return kaffare;
    }

    private String convertKaffareType(int i) {
        if (i == 0) {
            return "عمد";
        } else {
            return "عذر";
        }
    }

    public Fetrie fetrieToString(Context context, ServerFetrie serverFetrie) {
        Fetrie fetrie = new Fetrie();

        if(serverFetrie.getCost()>0) {
            fetrie.setCost(serverFetrie.getCost() + "");
        }else{
            fetrie.setCost("-");
        }
        fetrie.setFoodType(convertFoodType(context, serverFetrie.getFoodType()));
        if(serverFetrie.getInstitude()>=0) {
            fetrie.setInstitude(context.getResources().getStringArray(R.array.institudes)[serverFetrie.getInstitude()]);
        }else{
            fetrie.setInstitude("-");
        }
        fetrie.setIsSeyyed(convertIsSeyyed(context, serverFetrie.isSeyyed()));
        if (serverFetrie.isSeyyed()) {
            fetrie.setPaytoSeyyed(convertPayToSeyyed(context, serverFetrie.isPaytoSeyyed()));
        } else {
            fetrie.setPaytoSeyyed("-");
        }
        fetrie.setProvince(serverFetrie.getProvince());
        if(serverFetrie.getPersonNum()>0) {
            fetrie.setPersonNum(serverFetrie.getPersonNum() + "");
        }else{
            fetrie.setPersonNum("-");
        }
        fetrie.setCity(serverFetrie.getCity());

        return fetrie;
    }

    private String convertPayToSeyyed(Context context, boolean paytoSeyyed) {
        if (paytoSeyyed) {
            return context.getResources().getString(R.string.yes);
        } else {
            return context.getResources().getString(R.string.no);
        }
    }

    private String convertIsSeyyed(Context context, boolean seyyed) {
        if (seyyed) {
            return context.getResources().getString(R.string.seyyed);
        } else {
            return context.getResources().getString(R.string.notSeyyed);
        }
    }

    private String convertFoodType(Context context, int foodType) {
        if (foodType == 0) {
            return context.getResources().getString(R.string.wheat);
        } else if (foodType == 1) {
            return context.getResources().getString(R.string.rice);
        } else {
            return "";
        }
    }

    private static G ourInstance = new G();

    public static G getInstance() {
        return ourInstance;
    }

    private G() {
    }
}
