package com.sepehr.hoshmand.fetrie.object;

/**
 * Created by saliii on 5/29/17.
 */
public class Fetrie {

    private String foodType;
    private String isSeyyed;
    private String paytoSeyyed;
    private String cost;
    private String province;
    private String personNum;
    private String institude;
    private String city;

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public String getIsSeyyed() {
        return isSeyyed;
    }

    public void setIsSeyyed(String isSeyyed) {
        this.isSeyyed = isSeyyed;
    }

    public String getPaytoSeyyed() {
        return paytoSeyyed;
    }

    public void setPaytoSeyyed(String paytoSeyyed) {
        this.paytoSeyyed = paytoSeyyed;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPersonNum() {
        return personNum;
    }

    public void setPersonNum(String personNum) {
        this.personNum = personNum;
    }

    public String getInstitude() {
        return institude;
    }

    public void setInstitude(String institude) {
        this.institude = institude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
