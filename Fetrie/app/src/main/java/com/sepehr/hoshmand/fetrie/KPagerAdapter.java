package com.sepehr.hoshmand.fetrie;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sepehr.hoshmand.fetrie.fragments.FactorFragment;
import com.sepehr.hoshmand.fetrie.fragments.TypeFragment;
import com.sepehr.hoshmand.fetrie.fragments.kaffare.KDaysFragment;
import com.sepehr.hoshmand.fetrie.fragments.kaffare.KInstitudeFragment;
import com.sepehr.hoshmand.fetrie.fragments.kaffare.KPaymentFragment;
import com.sepehr.hoshmand.fetrie.fragments.kaffare.KPriceFragment;
import com.sepehr.hoshmand.fetrie.fragments.kaffare.KProvinceFragment;
import com.sepehr.hoshmand.fetrie.fragments.kaffare.KStartFragment;
import com.sepehr.hoshmand.fetrie.fragments.kaffare.KTypeFragment;

import java.util.HashMap;

public class KPagerAdapter extends FragmentPagerAdapter {


    private Context context;

    HashMap<Integer,Fragment> fragments;

    public KPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        fragments = new HashMap<>();
    }

    @Override
    public Fragment getItem(int position) {

        if(position==7){
            Fragment f = KStartFragment.newInstance();
            fragments.put(7,f);
            return f;
        }if(position==6){
            Fragment f = KTypeFragment.newInstance();
            fragments.put(6,f);
            return f;
        }if(position==5){
            Fragment f = KDaysFragment.newInstance();
            fragments.put(5,f);
            return f;
        }if(position==4){
            Fragment f = KProvinceFragment.newInstance();
            fragments.put(4,f);
            return f;
        }
        if(position==3){
            Fragment f = KPriceFragment.newInstance();
            fragments.put(3,f);
            return f;
        }if(position==2){
            Fragment f = KInstitudeFragment.newInstance();
            fragments.put(2,f);
            return f;
        }if(position==1){
            Fragment f = KPaymentFragment.newInstance();
            fragments.put(1,f);
            return f;
        }if(position==0){
            Fragment f = FactorFragment.newInstance();
            fragments.put(0,f);
            return f;
        }else{
            Fragment f = TypeFragment.newInstance();
            fragments.put(-1,f);
            return f;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return context.getResources().getStringArray(R.array.kaffare_titles)[position];
    }

    @Override
    public int getCount() {
        return 8;
    }

    public Fragment getItemWithPosition(int position) {
        return fragments.get(position);
    }
}