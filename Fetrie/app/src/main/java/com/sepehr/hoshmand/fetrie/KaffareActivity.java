package com.sepehr.hoshmand.fetrie;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.sepehr.hoshmand.fetrie.fragments.FactorFragment;
import com.sepehr.hoshmand.fetrie.fragments.PaymentFragment;
import com.sepehr.hoshmand.fetrie.fragments.kaffare.KPaymentFragment;
import com.viewpagerindicator.TitlePageIndicator;

import ir.fanap.psp.fanapinapppayment.model.FanapResponsePay;

public class KaffareActivity extends AppCompatActivity {


    private KPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private TitlePageIndicator tabPageIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kaffare);
        mSectionsPagerAdapter = new KPagerAdapter(getSupportFragmentManager(),this);

        mViewPager = (ViewPager) findViewById(R.id.container);
        tabPageIndicator = (TitlePageIndicator) findViewById(R.id.tab_pager);

        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabPageIndicator.setViewPager(mViewPager);

        mViewPager.setCurrentItem(7);


    }

    public void gotoNextPage(){
        if (mViewPager.getCurrentItem() > 0) {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
        }
    }


    public void goto2NextPage() {
        if (mViewPager.getCurrentItem() > 1) {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 2);
        }
    }

    public void updatePaymentFragment() {
        ((KPaymentFragment)mSectionsPagerAdapter.fragments.get(1)).setValues();
    }

    public void setFactor(FanapResponsePay response) {
        ((FactorFragment)mSectionsPagerAdapter.fragments.get(0)).setResponse(response);
    }
}
