package com.sepehr.hoshmand.fetrie.object;

/**
 * Created by saliii on 5/29/17.
 */
public class ServerFetrie {

    private int foodType;
    private boolean isSeyyed;
    private boolean paytoSeyyed;
    private int cost;
    private String province;
    private String city;
    private int personNum;
    private int institude;


    public int getFoodType() {
        return foodType;
    }

    public void setFoodType(int foodType) {
        this.foodType = foodType;
    }

    public boolean isSeyyed() {
        return isSeyyed;
    }

    public void setSeyyed(boolean seyyed) {
        isSeyyed = seyyed;
    }

    public boolean isPaytoSeyyed() {
        return paytoSeyyed;
    }

    public void setPaytoSeyyed(boolean paytoSeyyed) {
        this.paytoSeyyed = paytoSeyyed;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public int getPersonNum() {
        return personNum;
    }

    public void setPersonNum(int personNum) {
        this.personNum = personNum;
    }

    public int getInstitude() {
        return institude;
    }

    public void setInstitude(int institude) {
        this.institude = institude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
